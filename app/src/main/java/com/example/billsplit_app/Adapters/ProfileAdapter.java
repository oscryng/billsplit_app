package com.example.billsplit_app.Adapters;

import static androidx.core.content.ContentProviderCompat.requireContext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.media.MediaTimestamp;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.billsplit_app.InternalFiles;
import com.example.billsplit_app.MainActivity;
import com.example.billsplit_app.R;
import com.example.billsplit_app.Screens.EvenBillScreen;
import com.example.billsplit_app.Screens.FinalScreen;
import com.example.billsplit_app.Screens.IndividualBillScreen;
import com.example.billsplit_app.User;

import org.json.JSONException;

import java.util.ArrayList;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder>{

    private Context context;
    TextView totalTextView;

    public ProfileAdapter(@NonNull Context context, TextView currentTotalText){
        this.context = context;
        this.totalTextView = currentTotalText; // having this here and in the parameters means I can update stuff in other screens through code in the adapter
    }

    public class ProfileViewHolder extends RecyclerView.ViewHolder{
        private TextView name_str;
        private ImageButton profile_background;
        private TextView profile_short_user_name;

        public ProfileViewHolder(@NonNull View itemView) {
            super(itemView);
            profile_background = itemView.findViewById(R.id.profile_background);
            name_str = itemView.findViewById(R.id.profile_user_name);
            profile_short_user_name = itemView.findViewById(R.id.profile_short_user_name);
        }
    }

    @NonNull
    @Override
    public ProfileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_profile, parent,false);
        return new ProfileViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileViewHolder holder, int position) {
        User currentUser = MainActivity.usersList.get(position);
        String name = currentUser.getUsername();
        holder.name_str.setText(name);
        holder.profile_background.getBackground().setTint(currentUser.getColor());
        if (!name.isEmpty()) {
            holder.profile_short_user_name.setText(name.substring(0,1));
        }

        holder.profile_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowPopup(v,currentUser);
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainActivity.usersList.size();
    }

    public void ShowPopup(View view, User user) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View popupView = inflater.inflate(R.layout.change_remove_profile_popup, null, false);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        ImageView background = popupView.findViewById(R.id.popup_background);
        ImageButton closePopup = popupView.findViewById(R.id.popup_close_button);
        Button delete = popupView.findViewById(R.id.delete);
        Button edit = popupView.findViewById(R.id.edit);
        EditText editName = popupView.findViewById(R.id.new_name);

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // This is just here to prevent the popup from closing when clicking the background
            }
        });

        closePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NotifyDataSetChanged", "SetTextI18n", "DefaultLocale"})
            @Override
            public void onClick(View v) { // TODO there can be lots of easy code simplifications here by moving some stuff around
                if (MainActivity.check()) { // even bill screen
                    View currentFocus = ((EvenBillScreen)context).getCurrentFocus(); // solves a bug with crashing when deleting a profile that we add a tip for using keyboard
                    if (currentFocus != null) {
                        currentFocus.clearFocus();
                    }
                    notifyItemRemoved(MainActivity.usersList.indexOf(user));
                    ((EvenBillScreen)context).notifyTipAdapterOfRemoval(user);
                    MainActivity.set_user_count(MainActivity.nOfUsers-1);
                    MainActivity.usersList.remove(user);
//                    for (User user : MainActivity.usersList) {
//                        try {
//                            user.refreshTotalEven();
//                        } catch (JSONException e) {
//                            throw new RuntimeException(e);
//                        }
//                    }
                    try {
                        MainActivity.finalTipTotal = ((EvenBillScreen)context).updateTipsPercentage();
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }
                else { // individual bill screen
                    View currentFocus = ((IndividualBillScreen)context).getCurrentFocus();
                    if (currentFocus != null) {
                        currentFocus.clearFocus();
                    }
                    user.getSharedDishes().clear();
                    notifyItemRemoved(MainActivity.usersList.indexOf(user));
                    ((IndividualBillScreen)context).notifySharedAdapterOfRemoval(user);
                    MainActivity.set_user_count(MainActivity.nOfUsers-1);
                    MainActivity.usersList.remove(user);
                }
                popupWindow.dismiss();
                try {
                    totalTextView.setText("$ " + String.format("%.2f", InternalFiles.getSavedCost() + MainActivity.finalTipTotal));
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                Log.d("totaltextviewerror", "line 145 profileadapter is setting to " + String.format("%.2f",MainActivity.finalTipTotal));
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.usersList.get(MainActivity.usersList.indexOf(user)).setUsername(editName.getText().toString());
                notifyItemChanged(MainActivity.usersList.indexOf(user));
                if (MainActivity.check()) {
                    ((EvenBillScreen)context).refreshAdapters();
                }
                else {
                    ((IndividualBillScreen)context).refreshAdapters();
                }
                popupWindow.dismiss();
            }
        });
    }
}
