package com.example.billsplit_app.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.billsplit_app.Dish;
import com.example.billsplit_app.InternalFiles;
import com.example.billsplit_app.MainActivity;
import com.example.billsplit_app.R;
import com.example.billsplit_app.User;

import org.json.JSONException;

import java.util.Objects;

public class SharedAdapter extends RecyclerView.Adapter<SharedAdapter.SharedViewHolder>{
    private Dish adapterDish;
    public int num;

    public SharedAdapter() {
    }

    public class SharedViewHolder extends RecyclerView.ViewHolder{
        private TextView shared_profile_user_name;
        private ImageView shared_profile_background;
        private TextView shared_profile_short_user_name;
        private ImageView shared_checkmark;
        private TextView stuff;

        public SharedViewHolder(@NonNull View itemView) {
            super(itemView);
            shared_profile_background = itemView.findViewById(R.id.shared_profile_background);
            shared_profile_user_name = itemView.findViewById(R.id.shared_profile_user_name);
            shared_profile_short_user_name = itemView.findViewById(R.id.shared_profile_short_user_name);
            shared_checkmark = itemView.findViewById(R.id.shared_checkmark);
            stuff = itemView.findViewById(R.id.stuff);
            stuff.setText(Integer.toString(MainActivity.dishList.size()-1));
        }
    }

    @NonNull
    @Override
    public SharedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shared_profile, parent,false);
        return new SharedViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SharedViewHolder holder, int position) {

        User currentUser = MainActivity.usersList.get(position);
        String username = currentUser.getUsername();
        holder.shared_profile_user_name.setText(username);
//        GradientDrawable gd = new GradientDrawable();
//        gd.setColor(Color.parseColor("#f9f9f9"));
//        gd.setShape(GradientDrawable.OVAL);
//        gd.setStroke(2,Color.parseColor("#D1D1D1"));
//        holder.shared_profile_background.setBackground(gd);
        holder.shared_profile_background.getBackground().setTint(currentUser.getColor());
        holder.shared_profile_short_user_name.setTextColor(Color.WHITE);
        holder.shared_checkmark.setVisibility(View.INVISIBLE);

        if (!username.isEmpty()) {
            holder.shared_profile_short_user_name.setText(username.substring(0,1));
        }

//        if (MainActivity.newUserAdded) {
//            boolean result = MainActivity.usersList.isEmpty();
//            boolean result2 = MainActivity.dishList.isEmpty();
//            boolean result3 = MainActivity.dishNameList.isEmpty();
//        }

        Log.d("shareddisheserror", "sharedadapter's onbindviewholder has been refreshed");

//        for (User user : MainActivity.usersList) {
//        if (!currentUser.getSharedDishes().contains(adapterDish)) {
//            holder.shared_checkmark.setVisibility(View.INVISIBLE);
//            Log.d("shareddisheserror", String.format("line 82 sharedadapter user %s doesn't have %s", currentUser.getUsername(), adapterDish.getName()));
//        }
//        else {
//            holder.shared_checkmark.setVisibility(View.VISIBLE);
//            Log.d("shareddisheserror", String.format("line 82 sharedadapter user %s has %s", currentUser.getUsername(), adapterDish.getName()));
//        }
//            if (user.getSharedDishes().contains(adapterDish)) {
//                holder.shared_checkmark.setVisibility(View.VISIBLE);
//            }
//            else {
//                holder.shared_checkmark.setVisibility(View.INVISIBLE);
//            }
//        }




        holder.shared_profile_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.shared_checkmark.getVisibility() == View.VISIBLE) {
                    System.out.println(holder.stuff.getText().toString());
//                    GradientDrawable gd = new GradientDrawable();
//                    gd.setColor(Color.parseColor("#f9f9f9"));
//                    gd.setShape(GradientDrawable.OVAL);
//                    gd.setStroke(2,Color.parseColor("#D1D1D1"));
//                    holder.shared_profile_background.setBackground(gd);
//                    holder.shared_profile_short_user_name.setTextColor(Color.BLACK);
                    holder.shared_checkmark.setVisibility(View.INVISIBLE);
//                    MainActivity.dishList.get(Integer.parseInt(holder.stuff.getText().toString())).removeUser(currentUser);
//                    currentUser.removeDish(adapterDish);
                }
                else {
//                    boolean result = MainActivity.usersList.isEmpty();
//                    boolean result2 = MainActivity.dishList.isEmpty();
//                    boolean result3 = MainActivity.dishNameList.isEmpty();
                    System.out.println(holder.stuff.getText().toString());
//                    holder.shared_profile_short_user_name.setTextColor(Color.WHITE);
                    holder.shared_checkmark.setVisibility(View.VISIBLE);
//                    MainActivity.dishList.get(Integer.parseInt(holder.stuff.getText().toString())).addUser(currentUser);
//                    currentUser.addDish(adapterDish);
                }
//                currentUser.refreshTotal();
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainActivity.usersList.size();
    }

    public void setAdapterDish(Dish dish) {
        adapterDish = dish;
        System.out.println(dish);
    }

    public void setAllColour(SharedViewHolder holder) {
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(Color.parseColor("#f9f9f9"));
        gd.setShape(GradientDrawable.OVAL);
        gd.setStroke(2,Color.parseColor("#D1D1D1"));
        holder.shared_profile_background.setBackground(gd);
        holder.shared_profile_short_user_name.setTextColor(Color.BLACK);
        holder.shared_checkmark.setVisibility(View.INVISIBLE);
    }
}
